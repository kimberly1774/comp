﻿using DMS.Geometry;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    class HelperBall
    {
        public Box2D box;
        public Vector2 pos;
        public Vector2 dir;

        public HelperBall(float X, float Y, float sizeX, float sizeY)
        {
            pos = new Vector2(X, Y);
            box = new Box2D(X, Y, sizeX, sizeY);
            dir = new Vector2(0.0f, 0.03f);
        }

        public void update()
        {
            box.X += dir.X;
            box.Y += dir.Y;
        }
    }
}
